package at.campus02.nowa.swd;

import java.util.ArrayList;
import java.util.List;

public class Stall {
	
	private List<Duck> plätze = new ArrayList<>();
	
	public void addDuck(Duck d) {
		plätze.add(d);
	}
	
	public void enter() {
		for (Duck duck : plätze) {
			System.out.println(duck.quack());
			System.out.println(duck.fly());
		}
	}

	public static void main(String[] args) {
		Stall s = new Stall();
		s.addDuck(new MallardDuck());
		s.addDuck(new MallardDuck());
		s.addDuck(new MallardDuck());
		s.addDuck(new TurkeyAdapter(new WildTurkey()));
		s.enter();
	}

}
