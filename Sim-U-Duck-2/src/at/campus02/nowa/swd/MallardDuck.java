package at.campus02.nowa.swd;

public class MallardDuck implements Duck {

	@Override
	public String fly() {
		return "Fliegt schön";
	}

	@Override
	public String quack() {
		return "Quackt melodisch";
	}

}
