package at.campus02.nowa.swd;

public interface Turkey {

	public String gobble();

	public String fly();
}
