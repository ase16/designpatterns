package at.campus02.nowa.swd;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Verlag {
	
	private Map<String, Set<Abonnent>> abonnenten = new HashMap<>();
	
	public static void main(String[] args) {
		Verlag styriamedia = new Verlag();
		styriamedia.neueZeitschrift("NG");
		styriamedia.neueZeitschrift("GM");
		styriamedia.abonnieren(new Abonnent("Hugo"), "GM");
		styriamedia.abonnieren(new Abonnent("Hansi"), "NG");
		styriamedia.abonnieren(new Abonnent("Hansi"), "GM");
		styriamedia.abonnieren(new Abonnent("Karl"), "GM");
		styriamedia.abonnieren(new Abonnent("Franz"), "GM");
		styriamedia.abonnieren(new Abonnent("Susi"), "NG");
		styriamedia.neueAusgabe("GM", "Jänner Ausgabe");
		styriamedia.neueAusgabe("NG", "Neues vom Baby");
	}
	
	public void neueZeitschrift(String name) {
		abonnenten.put(name, new HashSet<>());
	}
	
	public void neueAusgabe(String zeitschrift, String titel) {
		if (abonnenten.containsKey(zeitschrift)) {
			for (Abonnent abonnent : abonnenten.get(zeitschrift)) {
				abonnent.empfangen(titel, zeitschrift);
			}
		}
	}
	
	public void abonnieren(Abonnent abonnent, String zeitschrift) {
		if (abonnenten.containsKey(zeitschrift)) {
			abonnenten.get(zeitschrift).add(abonnent);
			abonnent.empfangen("Willkomensausgabe", zeitschrift);
		}
		
	}
}
