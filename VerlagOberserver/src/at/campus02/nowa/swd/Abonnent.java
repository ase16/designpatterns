package at.campus02.nowa.swd;

public class Abonnent {
	
	private String name;
	
	
	
	public Abonnent(String name) {
		super();
		this.name = name;
	}

	public void empfangen(String titel, String zeitschrift) {
		System.out.println(name + ": Ausgabe " + titel + " von " + zeitschrift + " empfangen!");
	}

}
