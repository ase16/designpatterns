package at.campus02.nowa.swd.command;

public class ToggleCommand extends AbstractCommand {
	
	private boolean on;

	public ToggleCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		on = tv.isOn();
		if (on) {
			tv.turnOff();
		} else {
			tv.turnOn();
		}
	}

	@Override
	public void undo() {
		if (on) {
			tv.turnOn();
		} else {
			tv.turnOff();
		}
	}

}
