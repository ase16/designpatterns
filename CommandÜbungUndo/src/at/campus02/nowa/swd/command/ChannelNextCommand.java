package at.campus02.nowa.swd.command;

public class ChannelNextCommand extends AbstractCommand {
	
	private int channel;

	public ChannelNextCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		channel = tv.getChannel();
		tv.setChannel((channel + 1) % tv.getMaxChannels());
	}

	@Override
	public void undo() {
		tv.setChannel(channel);

	}

}
