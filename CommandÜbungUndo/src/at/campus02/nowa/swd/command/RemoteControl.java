package at.campus02.nowa.swd.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class RemoteControl {

	private Map<String, Command> commands = new HashMap<String, Command>();
	private Stack<Command> history = new Stack<Command>();

	public RemoteControl(TV tv) {
		commands.put("+", new VolumeUpCommand(tv));
		commands.put("-", new VolumeDownCommand(tv));
		commands.put("n", new ChannelNextCommand(tv));
		commands.put("p", new ChannelPreviousCommand(tv));
		commands.put("o", new ToggleCommand(tv));
		commands.put("m", new MuteCommand(tv));
	}

	public void pushButton(String key) {
		if (commands.containsKey(key)) {
			Command command = commands.get(key);
			command.execute();
			history.push(command.clone());
		} else {
			undo();
		}
	}

	public void undo() {
		if (!history.empty()) {
			Command command = history.pop();
			command.undo();
		}
	}

	public void undoAll() {
		while (history.size() > 0) {
			undo();
		}
	}

}
