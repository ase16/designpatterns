package at.campus02.nowa.swd.command;

public class ChannelPreviousCommand extends AbstractCommand {
	
	private int channel;

	public ChannelPreviousCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		channel = tv.getChannel();
		tv.setChannel(((channel - 1) % tv.getMaxChannels()) - ((channel - 1) < 0 ? tv.getMaxChannels() : 0));
	}

	@Override
	public void undo() {
		tv.setChannel(channel);

	}

}
