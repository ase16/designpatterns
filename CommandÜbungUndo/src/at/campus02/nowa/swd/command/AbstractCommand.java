package at.campus02.nowa.swd.command;

public abstract class AbstractCommand implements Command {

	protected TV tv;

	public AbstractCommand(TV tv) {
		this.tv = tv;
	}

	@Override
	public Command clone() {
		try {
	         return (Command) super.clone();
	      } catch (CloneNotSupportedException e) {
	         e.printStackTrace();
	         return null;
	      }
	}

}
