package at.campus02.nowa.swd.command;

public interface Command extends Cloneable {
	
	void execute();
	void undo();
	Command clone();

}
