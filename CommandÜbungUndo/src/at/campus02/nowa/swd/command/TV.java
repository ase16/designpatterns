package at.campus02.nowa.swd.command;

public class TV {

	private int channel = 0;
	private int volume = 0;
	private int maxChannels;
	private int maxVolume = 100;
	private int minVolume = 0;
	
	private boolean on = false;



	public TV(int maxChannels) {
		this.maxChannels = maxChannels;
	}

	public int getChannel() {
		return channel;
	}

	public int getMaxChannels() {
		return maxChannels;
	}

	public int getMaxVolume() {
		return maxVolume;
	}

	public int getMinVolume() {
		return minVolume;
	}

	public int getVolume() {
		return volume;
	}

	public boolean isOn() {
		return on;
	}

	public void setChannel(int channel) {
		this.channel = channel;
		System.out.println("TV channel is now set to " + this.channel);
	}

	public void setVolume(int volume) {
		this.volume = volume;
		System.out.println("TV volume is now at " + this.volume);
	}

	public void turnOff() {
		System.out.println("Turning TV off ...");
		this.on = false;
	}

	public void turnOn() {
		System.out.println("Turning TV on ...");
		this.on = true;
	}

}
