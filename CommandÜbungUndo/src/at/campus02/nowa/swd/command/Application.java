package at.campus02.nowa.swd.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {

	public static void main(String[] args) throws IOException {
		TV tv = new TV(10);
		RemoteControl rc = new RemoteControl(tv);
		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);

		String CurLine = ""; // Line read from standard in

		while (!(CurLine.equals("q"))) {
			System.out.println("Einen Knopf dr�cken:");
			CurLine = in.readLine();
			if (!(CurLine.equals("q"))) {
				rc.pushButton(CurLine);
			}
		}
	}

}
