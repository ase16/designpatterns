package at.campus02.nowa.swd.command;

public class MuteCommand extends AbstractCommand {
	
	private int volume;
	private boolean mute = false;

	public MuteCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		if (mute) {
			tv.setVolume(volume);
			volume = tv.getMinVolume();
			mute = false;
		} else {
			volume = tv.getVolume();
			tv.setVolume(tv.getMinVolume());
			mute = true;
		}
	}

	@Override
	public void undo() {
		if (mute) {
			tv.setVolume(tv.getMinVolume());
		} else {
			tv.setVolume(volume);
		}
	}

}
