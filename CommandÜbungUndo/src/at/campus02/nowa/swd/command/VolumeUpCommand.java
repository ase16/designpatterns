package at.campus02.nowa.swd.command;

public class VolumeUpCommand extends AbstractCommand {
	
	private int volume;

	public VolumeUpCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		volume = tv.getVolume();
		if (volume < tv.getMaxVolume()) {
			tv.setVolume(volume + 1);
		}
	}

	@Override
	public void undo() {
		if (volume <= tv.getMaxVolume()) {
			tv.setVolume(volume);
		}
	}

}
