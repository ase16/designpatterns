package at.campus02.nowa.swd.command;

public class VolumeDownCommand extends AbstractCommand {
	
	private int volume;

	public VolumeDownCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		volume = tv.getVolume();
		if (volume > tv.getMinVolume()) {
			tv.setVolume(volume - 1);
		}
	}

	@Override
	public void undo() {
		if (volume >= tv.getMinVolume()) {
			tv.setVolume(volume);
		}
	}

}
