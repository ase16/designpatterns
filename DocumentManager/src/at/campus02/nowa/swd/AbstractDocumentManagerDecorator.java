package at.campus02.nowa.swd;

public abstract class AbstractDocumentManagerDecorator implements IDocumentManager {
	
	protected IDocumentManager decoratedDocumentManager;

	public AbstractDocumentManagerDecorator(IDocumentManager decoratedDocumentManager) {
		super();
		this.decoratedDocumentManager = decoratedDocumentManager;
	}

}
