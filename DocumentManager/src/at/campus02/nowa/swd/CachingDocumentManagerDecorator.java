package at.campus02.nowa.swd;

import java.util.HashMap;

public class CachingDocumentManagerDecorator extends AbstractDocumentManagerDecorator {

	private HashMap<String, String> cache = new HashMap<>();
	
	public CachingDocumentManagerDecorator(IDocumentManager decoratedDocumentManager) {
		super(decoratedDocumentManager);
	}

	@Override
	public String load(String filename) {
		if (cache.containsKey(filename)) {
			return cache.get(filename);
		}
		String content = decoratedDocumentManager.load(filename);
		cache.put(filename, content);
		return content;
	}

	@Override
	public void save(String filename, String content) {
		if (cache.containsKey(filename)) {
			cache.put(filename, content);
		}
		decoratedDocumentManager.save(filename, content);

	}
	
	public Integer getCacheSize() {
		return cache.size();
	}

}
