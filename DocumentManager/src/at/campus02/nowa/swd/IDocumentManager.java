package at.campus02.nowa.swd;

public interface IDocumentManager {
	public String load(String filename);
	public void save(String filename, String content);
}
