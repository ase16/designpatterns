package at.campus02.nowa.swd;

public class AuditingDocumentManagerDecorator extends AbstractDocumentManagerDecorator {

	public AuditingDocumentManagerDecorator(IDocumentManager decoratedDocumentManager) {
		super(decoratedDocumentManager);
	}

	@Override
	public String load(String filename) {
		System.out.println("Datei wird geladen: " + filename);
		return this.decoratedDocumentManager.load(filename);
	}

	@Override
	public void save(String filename, String content) {
		System.out.println("Datei wird gespeichert: " + filename);
		this.decoratedDocumentManager.save(filename, content);
	}

}
