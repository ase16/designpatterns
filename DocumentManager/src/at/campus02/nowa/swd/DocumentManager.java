package at.campus02.nowa.swd;

public class DocumentManager implements IDocumentManager {
	
	public String load(String filename) {
		return "";
	}
	
	public void save(String filename, String content) {
		
	}
	
	public static void main(String[] argv) {
		IDocumentManager dm1 = new DocumentManager();
		IDocumentManager dm2 = new AuditingDocumentManagerDecorator(dm1);
		IDocumentManager dm3 = new CachingDocumentManagerDecorator(dm2);
		String content = dm3.load("bla.txt");
		dm3.save("bla.txt", content + "x");
		System.out.println(((CachingDocumentManagerDecorator) dm3).getCacheSize());
	}
}
