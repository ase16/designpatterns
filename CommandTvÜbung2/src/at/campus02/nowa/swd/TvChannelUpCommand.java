package at.campus02.nowa.swd;

import java.util.Stack;

public class TvChannelUpCommand extends AbstractTvCommand {
	
	private Stack<Boolean> state = new Stack<>();

	public TvChannelUpCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		state.push(tv.powered());
		if (!tv.powered()) {
			tv.on();
		}
		tv.setChannel(tv.getChannel() % 40 + 1);
	}

	@Override
	public void undo() {
		if (tv.getChannel() == 1) {
			tv.setChannel(40);	
		} else {
			tv.channelDown();
		}
		if (!state.pop()) {
			tv.off();
		}
		
		
	}

}
