package at.campus02.nowa.swd;

public class TV implements Cloneable {
	
	private boolean powered = false;
	private int volume = 50;
	private int channel = 1;
	
	public void on() {
		System.out.println("TV powered on!");
		this.powered = true;
	}
	
	public void off() {
		System.out.println("TV powered off!");
		this.powered = false;
	}
	
	public boolean powered() {
		return this.powered;
	}
	
	public void volumeUp() {
		this.volume++;
		System.out.println("TV volume at " + volume);
	}
	
	public void volumeDown() {
		this.volume--;
		System.out.println("TV volume at " + volume);
	}

	public int getVolume() {
		return volume;
	}
	
	public void channelUp() {
		this.channel++;
		System.out.println("TV channel at " + channel);
	}
	
	public void channelDown() {
		this.channel--;
		System.out.println("TV channel at " + channel);
	}

	public int getChannel() {
		return channel;
	}

	public void setVolume(int volume) {
		this.volume = volume;
		System.out.println("TV volume at " + volume);
	}

	public void setChannel(int channel) {
		this.channel = channel;
		System.out.println("TV channel at " + channel);
	}
	
	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
