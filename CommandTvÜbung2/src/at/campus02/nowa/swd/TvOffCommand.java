package at.campus02.nowa.swd;

import java.util.Stack;

public class TvOffCommand extends AbstractTvCommand {
	
	private Stack<Boolean> state = new Stack<>();

	public TvOffCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		state.push(tv.powered());
		if (tv.powered()) {
			tv.off();
		}
	}

	@Override
	public void undo() {
		if (state.pop()) {
			tv.on();
		}
	}

}
