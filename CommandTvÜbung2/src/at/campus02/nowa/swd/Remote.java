package at.campus02.nowa.swd;

import java.util.HashMap;
import java.util.Stack;

public class Remote {

	private TV tv;
	private HashMap<Integer, Command> commands = new HashMap<>();
	private Stack<Command> history = new Stack<>();

	public Remote(TV tv) {
		this.tv = tv;
	}

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		TV tv = new TV();
		Remote remote = new Remote(tv);
		remote.setButtonCommand(1, new TvOnCommand(tv));
		remote.setButtonCommand(2, new TvOffCommand(tv));
		
		remote.setButtonCommand(3, new TvToggleCommand(tv));
		remote.setButtonCommand(4, new TvMuteCommand(tv));
		remote.setButtonCommand(5, new TvChannelUpCommand(tv));
		remote.setButtonCommand(6, new TvChannelDownCommand(tv));
		remote.button(1);
		remote.button(2);
		remote.button(1);
		remote.button(1);
		remote.button(1);
		remote.button(3);
		remote.button(3);
		remote.button(3);
		remote.button(2);
		remote.button(5);
		remote.button(6);
		remote.button(4);
		remote.button(4);
		remote.button(4);
		for (int i=0; i<20; i ++) {
			remote.undo();
		}

	}

	public void setButtonCommand(int button, Command command) {
		commands.put(button, command);
	}

	public void button(int button) {
		if (commands.containsKey(button)) {
			Command c = commands.get(button);
			c.execute();
			history.push(c);
		}
	}
	
	public void undo() {
		if (!history.isEmpty()) {
			history.pop().undo();
		}
	}


}
