package at.campus02.nowa.swd;

public abstract class AbstractTvCommand implements Command {

	protected TV tv;

	public AbstractTvCommand(TV tv) {
		super();
		this.tv = tv;
	}

}
