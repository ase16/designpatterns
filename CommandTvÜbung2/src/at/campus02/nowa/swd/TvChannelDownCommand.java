package at.campus02.nowa.swd;

import java.util.Stack;

public class TvChannelDownCommand extends AbstractTvCommand {
	
	private Stack<Boolean> state = new Stack<>();

	public TvChannelDownCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		state.push(tv.powered());
		if (!tv.powered()) {
			tv.on();
		}
		if (tv.getChannel() == 1) {
			tv.setChannel(40);
		} else {
			tv.channelDown();
		}
	}

	@Override
	public void undo() {
		tv.setChannel(tv.getChannel() % 40 + 1);
		if (!state.pop()) {
			tv.off();
		}
		
	}

}
