package at.campus02.nowa.swd;

public class TvToggleCommand extends AbstractTvCommand {
	
	public TvToggleCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		if (tv.powered()) {
			tv.off();
		} else {
			tv.on();
		}
	}

	@Override
	public void undo() {
		execute();
	}

}
