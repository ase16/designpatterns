package at.campus02.nowa.swd;

public class TvMuteCommand extends AbstractTvCommand {
	
	private int restore;
	private boolean muted = false;

	public TvMuteCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		if (!muted) {
			muted = true;
			restore = tv.getVolume();
			tv.setVolume(0);
		} else {
			muted = false;
			tv.setVolume(restore);
		}

	}

	@Override
	public void undo() {
		execute();
	}

}
