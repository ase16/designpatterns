package at.campus02.nowa.swd;

import java.util.Stack;

public class TvOnCommand extends AbstractTvCommand {
	
	private Stack<Boolean> state = new Stack<>();

	public TvOnCommand(TV tv) {
		super(tv);
	}

	@Override
	public void execute() {
		state.push(tv.powered());
		if (!tv.powered()) {
			tv.on();
		}
	}

	@Override
	public void undo() {
		if (!state.pop()) {
			tv.off();
		}
		
	}

}
