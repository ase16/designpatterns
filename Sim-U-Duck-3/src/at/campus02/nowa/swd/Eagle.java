package at.campus02.nowa.swd;

public interface Eagle {
	public String soar();
	public String screech();

}
