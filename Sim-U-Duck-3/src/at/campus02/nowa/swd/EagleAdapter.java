package at.campus02.nowa.swd;

public class EagleAdapter implements Duck {
	
	private Eagle eagle;

	public EagleAdapter(Eagle eagle) {
		super();
		this.eagle = eagle;
	}

	@Override
	public String fly() {
		return eagle.soar().toUpperCase();
	}

	@Override
	public String quack() {
		return eagle.screech();
	}

}
