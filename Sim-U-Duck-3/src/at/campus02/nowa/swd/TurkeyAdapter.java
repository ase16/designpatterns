package at.campus02.nowa.swd;

public class TurkeyAdapter implements Duck {
	
	private Turkey turkey;

	public TurkeyAdapter(Turkey turkey) {
		super();
		this.turkey = turkey;
	}

	@Override
	public String fly() {
		return "Tut so als ober schön fliegen könnte";
		//return turkey.fly();
	}

	@Override
	public String quack() {
		return turkey.gobble().replaceAll("laut", "leise");
	}

}
