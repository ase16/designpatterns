package at.campus02.nowa.swd;

import java.util.Date;

public class ProtocolStableDecorator extends AbstractStableDecorator {

	public ProtocolStableDecorator(IStable stable) {
		super(stable);
	}

	@Override
	public void enter() {
		System.out.println("Stall wird betreten: " + new Date());
		super.enter();
	}

	@Override
	public void leave() {
		super.leave();
		System.out.println("Stall wurde verlassen: " + new Date());
	}

}
