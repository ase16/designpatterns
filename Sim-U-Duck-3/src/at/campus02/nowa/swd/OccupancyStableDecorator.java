package at.campus02.nowa.swd;

public class OccupancyStableDecorator extends AbstractStableDecorator {

	public OccupancyStableDecorator(IStable stable, int maximum) {
		super(stable);
		this.maximum = maximum;
	}
	
	private int counter = 0;
	private int maximum;

	@Override
	public void addDuck(Duck duck) {
		if (counter < maximum) {
			super.addDuck(duck);
			counter++;
		} else {
			System.out.println("Stall ist voll!");
		}
	}

	@Override
	public void removeDuck(Duck duck) throws Exception {
		try {
			super.removeDuck(duck);
			counter--;
		} catch (Exception e) {
			System.out.println("Duck wurde nicht gelöscht: " + e.getMessage());
		}
		
	}

}
