package at.campus02.nowa.swd;

public interface IStable {
	public boolean isOccupied();
	public void enter();
	public void leave();
	public void addDuck(Duck duck);
	public void removeDuck(Duck duck) throws Exception;
}
