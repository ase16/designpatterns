package at.campus02.nowa.swd;

public interface Duck {

	public String fly();

	public String quack();
}
