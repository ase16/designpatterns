package at.campus02.nowa.swd;

public class LeaveStableCommand extends AbstractStableCommand {

	public LeaveStableCommand(IStable stable) {
		super(stable);
	}

	@Override
	public void execute() {
		if (stable.isOccupied()) {
			stable.leave();
		}
	}

}
