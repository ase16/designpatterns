package at.campus02.nowa.swd;

public abstract class AbstractStableDecorator implements IStable {

	protected IStable stable;
	
	public AbstractStableDecorator(IStable stable) {
		super();
		this.stable = stable;
	}

	@Override
	public boolean isOccupied() {
		return stable.isOccupied();
	}

	@Override
	public void enter() {
		stable.enter();
	}

	@Override
	public void leave() {
		stable.leave();
	}

	@Override
	public void addDuck(Duck duck) {
		stable.addDuck(duck);
	}

	@Override
	public void removeDuck(Duck duck) throws Exception {
		stable.removeDuck(duck);
	}

}
