package at.campus02.nowa.swd;

public abstract class AbstractStableCommand implements Command {
	
	protected IStable stable;

	public AbstractStableCommand(IStable stable2) {
		super();
		this.stable = stable2;
	}

}
