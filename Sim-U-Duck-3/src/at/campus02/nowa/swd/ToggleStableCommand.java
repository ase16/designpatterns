package at.campus02.nowa.swd;

public class ToggleStableCommand extends AbstractStableCommand {

	public ToggleStableCommand(IStable stable) {
		super(stable);
	}

	@Override
	public void execute() {
		if (stable.isOccupied()) {
			stable.leave();
		} else {
			stable.enter();
		}
	}

}
