package at.campus02.nowa.swd;

public class BaldEagle implements Eagle {
	public String soar() {
		return "Der Adler fliegt majestätisch!";
	}
	public String screech() {
		return "Ein anderer Vogel springt ein!";
	}
	
}
