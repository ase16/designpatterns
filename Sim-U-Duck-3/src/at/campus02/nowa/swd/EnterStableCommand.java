package at.campus02.nowa.swd;

public class EnterStableCommand extends AbstractStableCommand {

	public EnterStableCommand(IStable stable) {
		super(stable);
	}

	@Override
	public void execute() {
		if (!stable.isOccupied()) {
			stable.enter();
		}
	}

}
