package at.campus02.nowa.swd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Stable implements IStable {

	private boolean occupied = false;

	public boolean isOccupied() {
		return occupied;
	}

	private List<Duck> plätze = new ArrayList<>();

	public void addDuck(Duck d) {
		plätze.add(d);
	}

	public void removeDuck(Duck d) throws Exception {
		if (!plätze.remove(d)) {
			throw new Exception("Duck nicht im Stall: " + d);
		}
	}

	public void enter() {
		occupied = true;
		for (Duck duck : plätze) {
			System.out.println(duck.fly());
		}
	}

	public void leave() {
		for (Duck duck : plätze) {
			System.out.println(duck.quack());
		}
		occupied = false;
	}

	public static void main(String[] args) throws Exception {
		Stable so = new Stable();
		ProtocolStableDecorator sp = new ProtocolStableDecorator(so);
		DeathTrapStableDecorator sd = new DeathTrapStableDecorator(sp);
		OccupancyStableDecorator s = new OccupancyStableDecorator(sp, 3);
		Map<String, Command> commands = new HashMap<>();
		commands.put("enter", new EnterStableCommand(s));
		commands.put("leave", new LeaveStableCommand(s));
		commands.put("toggle", new ToggleStableCommand(s));
		BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
		String line;
		while ((line = r.readLine()) != null) {
			if (commands.containsKey(line)) {
				commands.get(line).execute();
			} else {
				Class c = Class.forName("at.campus02.nowa.swd." + line);
				Constructor cmd = c.getConstructor(IStable.class);
				((Command) cmd.newInstance(s)).execute();
			}
		}
	}

}
