package at.campus02.nowa.swd;

public interface Command {
	
	public void execute();

}
