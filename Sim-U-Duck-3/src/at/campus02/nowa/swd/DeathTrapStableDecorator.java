package at.campus02.nowa.swd;

public class DeathTrapStableDecorator extends AbstractStableDecorator {

	public DeathTrapStableDecorator(IStable stable) {
		super(stable);
	}

	@Override
	public void enter() {
		super.enter();
		System.out.println("Der Stall wird angezündet!");
	}

	@Override
	public void leave() {
		return;
	}
	

}
