package at.campus02.nowa.swd;

public class Caesar {
	
	private Integer key;

	public Caesar(Integer key) {
		super();
		this.key = key;
	}
	
	public int rotate(int in) {
		return (in - 65  + key) % 26 + 65; 
	}

}
