package at.campus02.nowa.swd;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

public class CaesarFilterReader extends FilterReader {
	
	private Caesar c;

	protected CaesarFilterReader(Reader arg0, int key) {
		super(arg0);
		this.c = new Caesar(key);
	}

	@Override
	public int read() throws IOException {
		int i = this.in.read();
		if (i < 0) {
			return i;
		}
		return c.rotate(i);
	}

	
}
