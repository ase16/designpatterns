package at.campus02.nowa.swd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class App {

	public static void main(String[] args) throws Exception {
		//Caesar c = new Caesar(25);
		//System.out.println(Character.toChars(c.rotate('K')));
		InputStream datei = new FileInputStream("geheim.txt");
		Reader reader = new InputStreamReader(datei);
		Reader secret = new CaesarFilterReader(reader, 13);
		int i;
		while ((i = secret.read()) >= 0) {
			System.out.print(Character.toChars(i));
		}
	}

}
